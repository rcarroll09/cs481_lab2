﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ListView
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible (false)]
    public partial class MainPage : ContentPage

    {
        ObservableCollection<sports> SportsCollection;
        public class sports
        {
            public string Title { get; set; }
            public string Title1 { get; set;}
            public string Img { get; set; }
            public string Img1 { get; set; }
        }

        public MainPage()
        {
            InitializeComponent();
            SportsCollection = new ObservableCollection<sports>() {
                new sports()
                {
                    Title="Football",
                    Title1 = "Recent Champ: Cheifs",
                    Img="football.jpeg",
                    Img1="football.jpeg",
                },
                new sports()
                {
                    Title="Soccer",
                    Title1 = "Recent Champ: Liverpool FC ",
                    Img="soccer.jpeg",
                    Img1="soccer1.jpeg",
                },
                new sports()
                {
                    Title="Baseball",
                    Title1 = "Recent Champ: Nationals",
                    Img="baseball.jpeg",
                    Img1="baseball1.jpeg",
                },
                new sports()
                {
                    Title="Basketball",
                    Title1 = "Recent Champs: Raptors",
                    Img="basketball.jpeg",
                    Img1="basketball1.jpeg",
                },
            };
            labTwo.ItemsSource = SportsCollection;

        }

        void Handle_Refreshing(System.Object sender, System.EventArgs e)
        {
            labTwo.IsRefreshing = false;
        }
    }
}
